The mirror is large so it is at https://nextcloud.owo69.me/index.php/s/8oFLAQN2eBnSFed

Also here's a copy of the code I used in case it goes missing or something
```js
var child_process = require("child_process");
var fs = require("fs");
var parser = require("fast-xml-parser");

child_process.execSync(`wget -nv http://releases.terra.vision/ -O ListBucketResult.xml`);

var xml = fs.readFileSync(`ListBucketResult.xml`, 'utf8');
var jso = parser.parse(xml);

var urls = jso.ListBucketResult.Contents.map(x => "http://releases.terra.vision/" + x.Key).filter(x => !x.endsWith("/"));

fs.writeFileSync("urllist.txt", urls.join("\n"));

child_process.spawnSync(`wget`, ["-nv", "-x", "-N", "-i", "urllist.txt"], {stdio:"inherit"});
```